package ru.kphu.itis.dataapp.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.kphu.itis.dataapp.model.entity.Person;
import ru.kphu.itis.dataapp.model.table.PersonTable;
import ru.kphu.itis.dataapp.model.wrapper.PersonWrapper;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String NAME = "data.db";

    public static final int VERSION = 1;


    public DatabaseHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(PersonTable.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    @NonNull
    public List<Person> selectPersons(){
        Cursor cursor = getReadableDatabase().rawQuery(PersonTable.getSelectQuery(), null);
        List<Person> persons = new PersonWrapper(cursor).getPersons();
        cursor.close();
        return persons;
    }

    public void insertPerson(@NonNull Person person){
        getWritableDatabase().execSQL(PersonTable.getInsertQuery(person));
    }

    public void clearPersons(){
        getWritableDatabase().execSQL(PersonTable.getDeleteQuery());
    }
}
