package ru.kphu.itis.dataapp.screen.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.kphu.itis.dataapp.R;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class MainViewHolder extends RecyclerView.ViewHolder {

    public TextView nameTextView;

    public TextView ageTextView;

    public TextView genderTextView;

    public MainViewHolder(View itemView) {
        super(itemView);

        nameTextView = itemView.findViewById(R.id.text_view_name);

        ageTextView = itemView.findViewById(R.id.text_view_age);

        genderTextView = itemView.findViewById(R.id.text_view_gender);
    }
}
