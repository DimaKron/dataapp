package ru.kphu.itis.dataapp.model.entity;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class Gender {
    public static final String MALE = "MALE";
    public static final String FEMALE = "FEMALE";
    public static final String UNDEFINED= "UNDEFINED";
}
