package ru.kphu.itis.dataapp.screen.main;

import android.app.Fragment;
import android.support.annotation.NonNull;

import ru.kphu.itis.dataapp.screen.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @NonNull
    @Override
    protected Fragment makeFragment() {
        return MainFragment.newInstance();
    }
}
