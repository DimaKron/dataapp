package ru.kphu.itis.dataapp.model.wrapper;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ru.kphu.itis.dataapp.model.entity.Person;
import ru.kphu.itis.dataapp.model.table.PersonTable;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class PersonWrapper extends CursorWrapper {

    public PersonWrapper(Cursor cursor) {
        super(cursor);
    }

    @NonNull
    public List<Person> getPersons(){
        List<Person> persons = new ArrayList<>();
        moveToFirst();
        while (!isBeforeFirst() && !isAfterLast()){
            persons.add(getPerson());
            moveToNext();
        }
        return persons;
    }

    @Nullable
    private Person getPerson(){
        if(!isBeforeFirst() && !isAfterLast()) {
            Person person = new Person();
            person.setName(getString(getColumnIndex(PersonTable.COLUMN_NAME)));
            person.setGender(getString(getColumnIndex(PersonTable.COLUMN_GENDER)));
            person.setAge(getInt(getColumnIndex(PersonTable.COLUMN_AGE)));
            return person;
        }
        return null;
    }
}
