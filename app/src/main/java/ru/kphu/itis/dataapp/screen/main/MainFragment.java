package ru.kphu.itis.dataapp.screen.main;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import ru.kphu.itis.dataapp.R;
import ru.kphu.itis.dataapp.model.database.DatabaseHelper;
import ru.kphu.itis.dataapp.model.entity.Gender;
import ru.kphu.itis.dataapp.model.entity.Person;
import ru.kphu.itis.dataapp.screen.base.BaseFragment;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class MainFragment extends BaseFragment {

    private RecyclerView recyclerView;

    private MainAdapter recyclerAdapter;

    private LinearLayoutManager layoutManager;

    private DatabaseHelper databaseHelper;

    @NonNull
    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        databaseHelper= new DatabaseHelper(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        layoutManager = new LinearLayoutManager(getActivity());

        recyclerAdapter = new MainAdapter();

        recyclerView = v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                recyclerAdapter.setPersons(databaseHelper.selectPersons());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_add:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        databaseHelper.insertPerson(new Person("Ivan Ivanov", Gender.MALE, 20));
                        recyclerAdapter.setPersons(databaseHelper.selectPersons());
                    }
                });
                break;
            case R.id.item_clear:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        databaseHelper.clearPersons();
                        recyclerAdapter.setPersons(databaseHelper.selectPersons());
                    }
                });
                break;
        }
        return true;
    }
}
