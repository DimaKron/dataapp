package ru.kphu.itis.dataapp.model.table;

import android.support.annotation.NonNull;

import ru.kphu.itis.dataapp.model.entity.Person;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class PersonTable {

    public static final String NAME = "persons";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_AGE = "age";

    public static final String COLUMN_GENDER = "gender";

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + NAME + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT NOT NULL, "
                + COLUMN_AGE + " INTEGER NOT NULL, "
                + COLUMN_GENDER + " TEXT NOT NULL "
                + ");";
    }

    @NonNull
    public static String getSelectQuery(){
        return "SELECT * FROM " + NAME + ";";
    }

    @NonNull
    public static String getInsertQuery(Person person){
        return "INSERT INTO " + NAME+ "("+ COLUMN_NAME +","+ COLUMN_AGE+ "," + COLUMN_GENDER + ") VALUES("
                +"\'" + person.getName() + "\',"
                + person.getAge() + ","
                +"\'" + person.getGender() + "\'"
                + ");";
    }

    @NonNull
    public static String getDeleteQuery(){
        return "DELETE FROM " + NAME + ";";
    }

}
