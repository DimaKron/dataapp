package ru.kphu.itis.dataapp.screen.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.kphu.itis.dataapp.R;
import ru.kphu.itis.dataapp.model.entity.Person;

/**
 * Created by Дмитрий on 16.10.2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {

    private List<Person> persons;

    public MainAdapter() {
        this.persons = new ArrayList<>();
    }

    public MainAdapter(@NonNull List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new MainViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        Person person = persons.get(position);

        holder.ageTextView.setText(String.valueOf(person.getAge()));

        holder.genderTextView.setText(person.getGender());

        holder.nameTextView.setText(person.getName());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public void setPersons(@NonNull List<Person> persons) {
        this.persons = persons;
        notifyDataSetChanged();
    }
}
